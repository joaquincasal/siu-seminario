#versiones:
```
groovy 2.4.12
grails 3.3.0
java-8-oracle
```


#integrar mysql:
- en build.gradle agregar:
```
dependencies {
    ...
    runtime "mysql:mysql-connector-java:5.1.27"
}

```
- crear base mysql con usuario correspondiente:
    Explicacion para hacer esto desde "mysql workbench"
    - crear base
    ```
        create database db_name;
    ```
    - crear usuario:
        - ir a server/user and priveleges
        - add account
        - ingresar "login name" y "password"
        - apply
        - seleccionar usuario
        - tab "schema and privileges"
        - add entry...
        - selected schema
        - elegir el esquema que usara la app + ok
        - seleccionar esquema y tocar "select all" y agregar "GRANT OPTION" que no lo agrega solo
        - apply
- asignar db a la app, modificar application.yml (se muestra solo development, pero debe hacerse en todos los entornos)
```
environments:
    development:
        dataSource:
          driverClassName: com.mysql.jdbc.Driver
          dialect: org.hibernate.dialect.MySQL5InnoDBDialect
          url: jdbc:mysql://<db_server>:<db_port>/<schema_name>?useUnicode=yes&characterEncoding=UTF-8
          username: <db_user>
          password: <db_pass>
```


#integrar migrations:
- en build.gradle agregar

```
buildscript {
    ...
    dependencies {
        ...
        classpath 'org.grails.plugins:database-migration:3.0.3'
    }
}
...
dependencies{
    ...
    compile 'org.liquibase:liquibase-core:3.5.3'
    compile 'org.grails.plugins:database-migration:3.0.3'
}

```
- crear changelog.xml
```
grails dbm-generate-changelog changelog.xml
```
se creara ./grails-app/migrations/changelog.xml
- en application.yml agregar:
```
grails:
    ...
    plugin:
        databasemigration:
            changelogFileName: changelog.xml
```
- crear tablas en la base de datos:
```
grails dbm-update
```
Esto crea dos tablas en la base de datos, DATABASECHANGELOG donde se registrar las migrations ya ejecutadas y DATABASECHANGLOGLOCK donde se registra si la base se encuentra bloqueada (y por quien)
- crear un delta:
Al crear una clase de domini ejecutar
```
grails dbm-gorm-diff NOMBRE_DELTA.xml
```
esto generara un archivo en grails-app/migrations/NOMBRE_DELTA.xml, para poder ejecutarlo es debe agregarse al changelog.xml
```
<databaseChangeLog ...>
    ...
    <include file="NOMBRE_DELTA.xml"/>
</databaseChangeLog...>
```
- correr deltas agregados al changelog.xml:
```
grails dbm-update
```


#integrar SpringSecurity (manejo de usuarios):
- en build.gradle agregar:
```
dependencies {
    ...
    compile 'org.grails.plugins:spring-security-core:3.2.0.M1'
}

```
- crear clases de usuario rol y clase intermedia:
```
grails s2-quickstart ubicacion UsuarioClass RoleClass
```
esto creara en grails-app/domain/ubicacion las clases UsuarioClass, RoleClass y UsuarioRoleClass.
ademas en src/main/groovt/ubicacion se crea UsuarioPasswordEncoderListener, esta clase se encarga de interceptar las altas de usuarios a la base y encriptar la pass.
por ultimo se crea grails-app/conf/application.groovy donde se definen los permisos para poder acceder a los distintos recursos
en este caso:
```
grails s2-quickstart guarani Usuario Rol
```
como estamos integramos migrations y esto ultimo creo tres clases de dominio nuevas (Usuario, Rol y UsuarioRol) creamos el delta para impactar el modelo en la base de datos
```
grails dbm-gorm-diff DELTA_001_USUARIO_Y_ROL.xml
```
luego agregamos el delta en changelog.xml
```
...
<databaseChangeLog ...>
    <include file="DELTA_001_USUARIO_Y_ROL.xml"/>
</databaseChangeLog>
```
y corremos la migracion
```
grails dbm-update
```