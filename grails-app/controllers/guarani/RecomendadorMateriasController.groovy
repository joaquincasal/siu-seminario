package guarani

import grails.converters.JSON

class RecomendadorMateriasController {

    static allowedMethods = [guardarPuntaje: "POST", guardarIntereses: "POST"]

    def recomendadorMateriasService

    def index(){
        redirect(action: 'misOpinionesEIntereses')
    }

    def puntuarMateria(){
        Set<Catedra> catedras = recomendadorMateriasService.getCatedrasParaOpinar()
        [catedras: catedras,
         idCatedraSeleccionada: params.getLong("catedraSeleccionadaId"),
         puntaje: params.getLong('puntaje'),
         comentario: params.comentario]
    }

    def guardarPuntaje(){
        Long catedraId = params.getLong("idCatedra")
        Long puntaje = params.getLong("puntaje")
        String comentario = params.comentario
        if(!catedraId || puntaje == null ||puntaje < 0 || puntaje > 5){
            render(view: 'puntajeMateria', model:[
                    catedraSeleccionadaId: catedraId,
                    puntaje: puntaje,
                    comentario: comentario
            ])
            return
        }
        Opinion nuevaOpinion = recomendadorMateriasService.guardarPuntaje(catedraId, puntaje, comentario)
        redirect(action: 'misOpinionesEIntereses', model:[nuevaOpinion: nuevaOpinion])
    }

    def elegirIntereses(){
        def intereses = recomendadorMateriasService.obtenerInteresesNoSeleccionados()
        [intereses:intereses]
    }

    def guardarIntereses(){
        List<Long> interesesId = params.getList("interesesId").collect{it -> new Long(it)}
        if (!recomendadorMateriasService.guardarIntereses(interesesId)){
            flash.message = "no se pudo guardar los intereses"
        }
        redirect(action: 'misOpinionesEIntereses')
    }

    def misOpinionesEIntereses(Opinion nuevaOpinion){
        Set<Opinion> misOpiniones = recomendadorMateriasService.obtenerMisOpiniones()
        Set<Interes> misIntereses = recomendadorMateriasService.obtenerMisIntereses()
        [misOpiniones: misOpiniones, nuevaOpinion: nuevaOpinion, misIntereses: misIntereses]
    }

    def recomendarmeMaterias(){
        [recomendaciones: recomendadorMateriasService.recomendaciones()]
    }
}
