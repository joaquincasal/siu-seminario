package guarani

class OrganizadorDeHorariosController {

    static allowedMethods = [elegirHorarios: ["POST", "GET"], recomendarHorarios: "POST"]

    def organizadorDeHorariosService

    def index() {
        redirect(action:'elegirPlanEstudio')
    }

    def elegirPlanEstudio(){
        Set<PlanEstudio> planesEstudio = estudianteLogeado.historiasAcademicas.collect {it.planEstudio}
        [planesEstudio: planesEstudio]
    }

    def elegirHorarios(){
        PlanEstudio planSeleccionado = PlanEstudio.get(params.getLong("planEstudioId"))
        Set<Horario> horarios = organizadorDeHorariosService.obtenerHorariosSemanales()
        Set<Materia> materias = estudianteLogeado.getMateriasHabilitadas(planSeleccionado).findAll{it.catedras}
        [horarios:horarios, materias: materias]
    }

    def recomendarHorarios(){
        if(!params.idMaterias || !params.horarios){
            flash.message = "falta seleccionar datos"
            redirect(action: 'elegirHorarios', model:[planEstudioId: params.planEstudioId])
            return
        }
        Set<Materia> materias = Materia.findAllByIdInList(params.getList('idMaterias').collect {new Long(it)})
        Set<Horario> horarios = Horario.findAllByIdInList(params.getList("horarios").collect {new Long(it)})
        Set<Set<Catedra>> catedras = organizadorDeHorariosService.organizarHorarios(horarios, materias)

        [catedras: catedras]
    }

    def springSecurityService

    private final Estudiante getEstudianteLogeado(){
        return Estudiante.findByUsuario((Usuario)springSecurityService.currentUser)
    }
}
