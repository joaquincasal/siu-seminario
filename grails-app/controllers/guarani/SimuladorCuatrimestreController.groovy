package guarani

class SimuladorCuatrimestreController {

    def simuladorCuatrimestreService
    def estudianteService

    static allowedMethods = [mostrarMaterias: "POST", simular: "POST"]

    def index() {
        render(view:'../organizadorDeHorarios/elegirPlanEstudio',
                model: [controllerReturn: 'simuladorCuatrimestre', actionReturn: 'mostrarMaterias',
                        planesEstudio: estudianteService.estudianteLogeado.historiasAcademicas.collect {it.planEstudio}])
    }

    def mostrarMaterias(){
        PlanEstudio planEstudio = PlanEstudio.get(params.getLong('planEstudioId'))
        Set<Materia> materiasAprobadas = simuladorCuatrimestreService.getMateriasAprobadas(planEstudio)
        Set<Materia> materiasHabilitadas = simuladorCuatrimestreService.getMateriasHabilitadas(planEstudio)
        Set<Materia> materiasRestantes = simuladorCuatrimestreService.getMateriasRestantes(planEstudio)
        [materiasAprobadas: materiasAprobadas,
         materiasHabilitadas: materiasHabilitadas,
         materiasRestantes: materiasRestantes,
         planEstudioId: planEstudio.id]
    }

    def simular(){
        PlanEstudio planEstudio = PlanEstudio.get(params.getLong('planEstudioId'))
        List<String> materiasAprobadasIdsStr = params.getList('materiasAprobadasIds')
        def materiasSeleccionadasIdsStr = params.materiasSeleccionadasIds.split(' ,')
        def idsMaterias = (materiasAprobadasIdsStr + materiasSeleccionadasIdsStr).flatten().collect {Long.parseLong(it)}
        Set<Materia> materiasAprobadas = Materia.findAllByIdInList(idsMaterias)
        Set<Materia> materiasHabilitadas = simuladorCuatrimestreService.getMateriasHabilitadas(planEstudio, materiasAprobadas)
        Set<Materia> materiasRestantes = simuladorCuatrimestreService.getMateriasRestantes(planEstudio, materiasAprobadas)

        def materiasDestrabadas = materiasHabilitadas - simuladorCuatrimestreService.getMateriasHabilitadas(planEstudio, Materia.findAllByIdInList(materiasAprobadasIdsStr.collect {new Long(it)}).toSet())
        render( view:'mostrarMaterias',
                model:[materiasRestantes: materiasRestantes,
                       materiasAprobadas: materiasAprobadas,
                       materiasHabilitadas: materiasHabilitadas,
                       planEstudioId: planEstudio.id,
                       seleccionadasAntes: materiasSeleccionadasIdsStr.collect{new Long(it)},
                       materiasNuevas: materiasDestrabadas
                ])

    }

}
