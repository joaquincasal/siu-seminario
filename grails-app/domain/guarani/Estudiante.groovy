package guarani

class Estudiante {

    String nombre
    String apellido
    Usuario usuario
    Long padron

    static hasMany = [
                        historiasAcademicas: HistoriaAcademica,
                        intereses: Interes,
                        opiniones: Opinion
    ]

    static mapping = {
        usuario cascade: "all"
    }

    static constraints = {
        nombre nullable: false, blank: false
        apellido nullable: false, blank: false
        usuario nullable: false, unique: true
        padron nullable: false, unique: true, min: 1L
    }

    static Estudiante crearEstudiante(String nombre, String apellido, Long padron, String usuario, String password) {
        Estudiante estudiante = new Estudiante()
        estudiante.nombre = nombre
        estudiante.apellido = apellido
        estudiante.padron = padron
        estudiante.usuario = new Usuario()
        estudiante.usuario.username = usuario
        estudiante.usuario.password = password
        Rol rol = Rol.findByAuthority('ROLE_ESTUDIANTE')
        estudiante.usuario.save(flush: true)
        estudiante.save(flush: true)
        UsuarioRol.create(estudiante.usuario, rol,  true)
        return estudiante
    }

    Boolean agregarCarrera(PlanEstudio planEstudio) {
        if (this.historiasAcademicas.any{ it.planEstudio })
            return false
        HistoriaAcademica historiaAcademica = new HistoriaAcademica(planEstudio: planEstudio, estudiante: this)
        this.addToHistoriasAcademicas(historiaAcademica)
        if (!this.save(flush: true, failOnError: true))
            return false
        return historiaAcademica.save(flush: true)
    }

    Set<Materia> getMateriasAprobadas() {
        return getMateriasAprobadas(this.historiasAcademicas.collect { it.planEstudio }.toSet())
    }

    Set<Materia> getMateriasAprobadas(PlanEstudio plan) {
        return getMateriasAprobadas([plan].toSet())
    }

    Set<Materia> getMateriasAprobadas(Set<PlanEstudio> planes){
        Set<HistoriaAcademica> historiasAcademicas = this.historiasAcademicas.findAll { HistoriaAcademica historia -> planes.contains(historia.planEstudio) }
        Set<ResultadoCursada> resultadosDeCursada = historiasAcademicas.collect{ it.resultadosCursadas }.flatten().toSet()
        Set<ResultadoCursada> cursadasAprobadas = resultadosDeCursada.findAll { ResultadoCursada resultadoDeCursada -> resultadoDeCursada.aprobada }
        Set<Materia> materiasAprobadas = cursadasAprobadas.collect{ it.catedra }.materia
        return materiasAprobadas
    }

    boolean chequearCorrelativas(PlanEstudio plan, Materia materia, Set<Materia> materiasAprobadas = getMateriasAprobadas([plan].toSet())) {
        Set<Materia> correlativasNecesarias = materia.getCorrelativas(plan)
        return materiasAprobadas.containsAll(correlativasNecesarias)
    }

    Set<Materia> getMateriasHabilitadas(PlanEstudio plan, Set<Materia> materiasAprobadas = getMateriasAprobadas([plan].toSet())) {
        Set<PlanMateria> planesMaterias = PlanMateria.findAllByPlanEstudio(plan)
        Set<Materia> materias = planesMaterias.collect { it.materia }.toSet()
        Set<Materia> materiasHabilitadas = materias.findAll { Materia materia -> this.chequearCorrelativas(plan, materia, materiasAprobadas) }
        return materiasHabilitadas - materiasAprobadas
    }

    Set<Materia> getMateriasNoHabilitadas(PlanEstudio plan, Set<Materia> materiasAprobadas = getMateriasAprobadas(plan)){
        Set<PlanMateria> planesMaterias = PlanMateria.findAllByPlanEstudio(plan)
        Set<Materia> materias = planesMaterias.collect { it.materia }.toSet()
        Set<Materia> materiasHabilitadas = getMateriasHabilitadas(plan, materiasAprobadas)
        Set<Materia> materiasNoHabilitadas = materias - materiasAprobadas - materiasHabilitadas
        return materiasNoHabilitadas
    }

    Set<Materia> getMateriasRestantes(){
        return this.historiasAcademicas.collect{ HistoriaAcademica historiaAcademica ->
            historiaAcademica.materiasRestantes
        }.flatten().toSet()
    }

    Set<Catedra> getCatedrasCursadasYFinalizadas(){
        Set<ResultadoCursada> resultadosDeCursada = this.historiasAcademicas.collect{ it.resultadosCursadas }.
                flatten().findAll { ResultadoCursada resultadoCursada ->
                    resultadoCursada.terminada
        }
        return resultadosDeCursada.collect{it.catedra}
    }

    Opinion opinar(Catedra catedra, Long puntaje, String comentario) {
        Boolean repiteOpinion = this.opiniones.any{ opinion ->
            opinion.catedra.equals(catedra)
        }
         if (repiteOpinion)
             throw new Exception("repite opinion para ${catedra.titular} - ${catedra.materia.nombre}") //TODO: RepiteOpinionException
        Opinion opinion =  Opinion.crear(catedra, puntaje, comentario, this)
        opinion.save(flush: true)
        return opinion
    }

    Boolean opinoDeLaCatedra(Catedra catedra) {
        return this.opiniones.any { opinion -> opinion.catedra == catedra}
    }

    Double puntuarEstudiante(Estudiante estudiante) {
        Double coeficienteInteres = this.coeficienteInteres
        Double puntajeInteres = this.calcularPuntajeInteres(estudiante)
        Double coeficienteOpinion = this.coeficienteOpinion
        Double puntajeOpinion = this.calcularPuntajeOpinion(estudiante)

        return coeficienteInteres * puntajeInteres + coeficienteOpinion * puntajeOpinion
    }

    Set<PuntajeEstudiante> puntuarEstudiantes(Set<Estudiante> estudiantes) {
        /***
         * Lista de mapas de dos claves: {estudiante: v1, puntaje: v2}
        ***/
        return estudiantes.inject([].toSet()) { puntajes ,estudiante ->
            Double puntaje = this.puntuarEstudiante(estudiante)
            puntajes << new PuntajeEstudiante(estudiante: estudiante, puntaje: puntaje)
        }
    }

    Double getCoeficienteInteres(){
        Long cantidadIntereses = this.intereses.size()
        if (!cantidadIntereses)
            return 0D
        /***
         * si el estudiante tiene marcados mas intereses que los "intereses de corte" entonces
         * el coeficiente de intereses es el coeficiente de interes, sino es 0
         * este coeficiente es siempre menor que 1 dado que representa el porcentaje de importancia de los interese
         * para el estudiante en cuestion
         * para esto se utiliza la ecucacion de un filtro pasa altos, la explicacion se encuentra en el informe
        ***/
        return ConstatnsProvider.COEFICIENTE_DE_INTERES * (1D / Math.sqrt(1 + Math.pow(10, ConstatnsProvider.INTERESES_DE_CORTE - this.intereses.size())))
    }

    Double getCoeficienteOpinion(){
        /***
         * es el complemente del coeficiente de interes,
         * la suma de estos dos coeficientes da siempre 1
         * ya que representa el 100%
        ***/
        return 1D - this.coeficienteInteres
    }

    Double calcularPuntajeInteres(Estudiante estudiante) {
        /***
         * Ij = Ic(B, Ej) / Ib
         * donde:
         *      Ij: puntaje por interes del estudiante j
         *      Ic(B, Ej): cantidad intereses comunes entre B y Ej
         *      Ib: cantidad de intereses de B
         *      B: estudiante buscador (quien busca recomendacion de materias)
         *      Ej: estudiante "j", con el que se busca saber que puntaje de importancia tiene
         * si Ib es:
         *      = 0  => el puntaje por interes es 0,
         *              independientemente de los intereses de Ej porque a B no le interesa nada
         *      != 0 => el puntaje puntaje es el cociente entre los intereses comunes y los intereses del buscador,
         *              en caso de que no haya intereses comunes la puntuacion es 0,
         *              en caso de que el estudiante j coincida en todos sus intereses con el buscador es 1
        ***/
        Long cantidadInteresesComunes = this.intereses.intersect(estudiante.intereses).size()
        Long cantidadIntereses = this.intereses.size()
        return cantidadIntereses ? cantidadInteresesComunes / cantidadIntereses : 0D
    }

    Double calcularPuntajeOpinion(Estudiante estudiante) {
        /***
         * Oj = {Sumatoria( 1 / [ di( Obi, Oeji ) + 1 ] ) } / Ccj si Ccj != 0
         * Oj = 0 si Ccj = 0
         * donde:
         *      Oj: puntaje por opiniones Ej
         *      Ccj: cantidad de catedras cursadas en comun entre B y Ej
         *      di: distancia entre opinion de Obi y Oeji en la catedra i, se calcula como | Obi - Oeji |
         *      Ob: puntaje de la opinion de B de la catedra i
         *      Oeji: puntaje de la opinion de Ej de la catedra i
         *      B: estudiante buscador (quien busca recomendacion de materias)
         *      Ej: estudiante "j", con el que se busca saber que puntaje de importancia tiene
         * Mientras mas cercanos los puntajes mas valor aporta al puntaje de opiniones,
         * en caso de coincidir di es 0 => aporta 1.
         * si B y Ej coinciden en todos los puntajes de las catedras que cursaron juntos entonces Oj = 1,
         *
        ***/
        Set<Catedra> misCatedras = this.catedrasCursadasYFinalizadas
        Set<Catedra> susCatedras = estudiante.catedrasCursadasYFinalizadas
        Set<Catedra> catedrasComunes = misCatedras.intersect(susCatedras)
        Long cantidadCatedrasComunes = catedrasComunes.size()
        if (!cantidadCatedrasComunes)
            return 0
        Double sumapuntajeOpiniones = catedrasComunes.inject(0D) { Double puntajeAcumulado, Catedra catedra ->
            Opinion miOpinion = this.opiniones.find { opinion -> opinion.catedra == catedra }
            Opinion suOpinion = estudiante.opiniones.find { opinion -> opinion.catedra == catedra }
            puntajeAcumulado += 1 / ( Math.abs( miOpinion.puntaje - suOpinion.puntaje ) + 1 )
            return puntajeAcumulado
        }
        return sumapuntajeOpiniones / cantidadCatedrasComunes
    }

    Boolean cursoEnCatedra(Catedra catedra){
        this.historiasAcademicas.resultadosCursadas.flatten().any{ ResultadoCursada resultadoCursada ->
            resultadoCursada.terminada && resultadoCursada.catedra == catedra
        }
    }

    Boolean opinoDeCatedra(Catedra catedra){
        return this.opiniones.any {it.catedra == catedra}
    }

    Opinion getOpinionParaCatedra(catedra){
        return this.opiniones.find { opinion -> opinion.catedra == catedra}
    }

    Boolean agregarMateriaAprobada(Long codigoCarrera, String titularCatedra, Materia materia, Long nota) {
        HistoriaAcademica historiaAcademica = this.historiasAcademicas.find{ ha ->
            ha.planEstudio.carrera.codigo == codigoCarrera
        }
        PlanEstudio plan = this.historiasAcademicas.collect{ it.planEstudio }.find{it.carrera.codigo == codigoCarrera}
        if (!plan || !plan.materias.any{it.materia == materia})
            return false
        Catedra catedra = plan.materias.collect{it.materia.catedras}.flatten().find{it.titular == titularCatedra && it.materia == materia}
        if (!catedra)
            return false
        ResultadoCursada resultadoCursada = new ResultadoCursada(catedra: catedra, terminada: true, aprobo: true, nota: nota)
        if (!resultadoCursada.save(flush: true))
            return false
        historiaAcademica.addToResultadosCursadas(resultadoCursada)
        return historiaAcademica.save(flush:true)
    }

    Boolean agregarOpinionCatedra(String titularCatedra, Materia materia, Long puntaje, String comentario) {
        Boolean terminoCatedra = this.historiasAcademicas.any{
            it.resultadosCursadas.any{it.catedra.titular  == titularCatedra && it.catedra.materia == materia && it.terminada}
        }
        if (!terminoCatedra)
            return false
        Catedra catedra = this.historiasAcademicas.collect {it.resultadosCursadas.collect {it.catedra}}.flatten().find{it.titular == titularCatedra}
        Opinion opinion = new Opinion(catedra: catedra, puntaje: puntaje, comentario: comentario)
        if (!opinion.save(flush: true))
            return false
        this.addToOpiniones(opinion)
        return this.save(flush: true)
    }

    Boolean agregarIntereses(Set<Interes> intereses) {
        intereses.collect{ Interes interes -> this.addToIntereses(interes) }
        return this.save(flush:true)
    }

}
