package guarani

import guarani.valueObject.RangoHorario

class Catedra {

    String titular

    static belongsTo = [materia: Materia]
    static hasMany = [horarios: Horario]
    static constraints = {
        horarios minSize: 1
    }

    static Catedra crearCatedra(String titular, ArrayList<LinkedHashMap<String, Serializable>> horariosListMap) {
        Set<Horario> horarios = horariosListMap.inject([].toSet()) {Set<Horario>acu, LinkedHashMap horarioMap ->
            Horario horario = new Horario(dia: horarioMap.dia, rango: new RangoHorario(horarioMap.rango.inicio, horarioMap.rango.fin))
            horario.save(flush:true)
            acu << horario
            return acu
        }
        Catedra catedra = new Catedra(titular: titular, horarios: horarios)
        catedra.save(flush: true)
        return catedra
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Catedra catedra = (Catedra) o

        if (id != catedra.id) return false

        return true
    }

    int hashCode() {
        return (id != null ? id.hashCode() : 0)
    }

    String getFullName(){
        return this.materia.nombre + " - " + this.titular
    }

    boolean seCursaEnElHorario(Horario horario) {
        def seCursa = horarios.find { horarioCursada -> horarioCursada.esParteDelHorario(horario)}
        return seCursa != null
    }

    boolean esCursableEnLosHorarios(Set<Horario> horariosDisponibles) {

        int cantidadDeHoras = this.horarios.inject(0) { total, horario -> total + horario.catidadDeHoras() }
        int horasPosibles = this.horarios.inject(0) { total, horario ->
            total + (horariosDisponibles.findAll { horario.esParteDelHorario(it) }).size()
        }

        return cantidadDeHoras == horasPosibles

    }

}
