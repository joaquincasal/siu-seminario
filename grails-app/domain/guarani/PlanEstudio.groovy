package guarani

class PlanEstudio {

    Long anio
    static hasMany = [materias: PlanMateria]
    static belongsTo = [carrera: Carrera]

    static constraints = {
        anio nullable: false, min: 1920L
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        PlanEstudio that = (PlanEstudio) o

        if (anio != that.anio) return false
        if (carrera != that.carrera) return false
        if (id != that.id) return false
        if (version != that.version) return false

        return true
    }

    int hashCode() {
        int result
        result = anio.hashCode()
        result = 31 * result + id.hashCode()
        result = 31 * result + version.hashCode()
        result = 31 * result + carrera.hashCode()
        return result
    }

    String getNombre(){
        return carrera.nombre + ' - ' + anio
    }
}
