package guarani

import groovy.transform.Sortable
import guarani.enums.Dia
import guarani.valueObject.RangoHorario

@Sortable(includes = ['dia', 'rango'])
class Horario {

    Dia dia
    RangoHorario rango
    boolean horarioDeCatedra = true

    static constraints = {
    }

    static embedded = ['rango']


    boolean esParteDelHorario(Horario horario) {
        boolean mismoDia = horario.dia == this.dia
        boolean entreElHorario = this.rango.pertenece(horario.rango)
        return mismoDia && entreElHorario
    }

    int catidadDeHoras() {
        return rango.cantidadDeHoras()
    }
}
