package guarani

class Interes {

    String tag

    static constraints = {
        tag nullable: false, blank: false, unique: true
    }
}
