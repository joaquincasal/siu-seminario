package guarani

class Opinion {

    Long puntaje
    String comentario
    Catedra catedra

    static constraints = {
        puntaje min: 1L, max: 5L
        comentario nullable: false, blank: true
        catedra nullable: false
    }

    private Opinion(){}

    static Opinion crear(Catedra catedra, Long puntaje, String comentario, Estudiante estudiante) {
        Opinion opinion = new Opinion()
        opinion.catedra = catedra
        opinion.puntaje = puntaje
        opinion.comentario = comentario
        estudiante.addToOpiniones(opinion)
        return opinion
    }

}
