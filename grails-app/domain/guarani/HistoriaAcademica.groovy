package guarani

class HistoriaAcademica {

    PlanEstudio planEstudio
    static belongsTo = [
                        estudiante: Estudiante
    ]

    static hasMany = [resultadosCursadas: ResultadoCursada]

    static constraints = {
    }

    public Set<Materia> getMateriasRestantes(){
        Set<Materia> materiasAprobadas = this.resultadosCursadas.findAll { ResultadoCursada resultadoCursada ->
            resultadoCursada.aprobada
        }.collect { ResultadoCursada resultadoCursada ->
            resultadoCursada.catedra.materia
        }
        return this.planEstudio.materias.materia.flatten() - materiasAprobadas
    }
}
