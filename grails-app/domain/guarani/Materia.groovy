package guarani

class Materia {

    Long codigo
    String nombre
    int creditos
    static hasMany = [catedras: Catedra]

    static constraints = {
        codigo unique: true, nullable: false, min: 1000L
        nombre nullable: false, blank: false
        creditos nullables: false, min: 1
    }

    def agregarCatedra(String titular, ArrayList<LinkedHashMap<String, Serializable>> horarios) {
        Catedra catedra = Catedra.crearCatedra(titular, horarios)
        this.addToCatedras(catedra)
        this.save(flush: true)
    }

    def getCorrelativas(PlanEstudio plan){
        PlanMateria planMateria = PlanMateria.findByPlanEstudioAndMateria(plan, this)
        Set<Materia> correlativas = planMateria.correlativas.toSet()
        return correlativas
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Materia materia = (Materia) o

        if (codigo != materia.codigo) return false
        if (id != materia.id) return false
        if (nombre != materia.nombre) return false

        return true
    }

    int hashCode() {
        int result
        result = (codigo != null ? codigo.hashCode() : 0)
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0)
        result = 31 * result + (id != null ? id.hashCode() : 0)
        return result
    }
}