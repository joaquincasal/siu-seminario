package guarani

class ResultadoCursada {

    Long nota
    Boolean terminada
    Catedra catedra

    static constraints = {
        nota min: 0L, max: 10L, nullable: false
        terminada nullable: false
        catedra nullable: false
    }

    boolean isAprobada() {
        return terminada && nota >= 4
    }

}
