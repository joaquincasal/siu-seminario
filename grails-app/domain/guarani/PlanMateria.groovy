package guarani

class PlanMateria {

    Materia materia
    boolean isObligatoria
    static belongsTo = [planEstudio: PlanEstudio]
    static hasMany = [correlativas: Materia]

    static constraints = {
        materia nullable: false
        isObligatoria nullable: false
    }

}
