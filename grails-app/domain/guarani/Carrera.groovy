package guarani

class Carrera {

    String nombre
    Long codigo
    static hasMany = [planEstudio: PlanEstudio]

    static constraints = {
        codigo nullable: false, unique: true
        nombre nullable: false, blank: false, unique: true
    }

    Boolean anotarEstudiante(Estudiante estudiante, Long anio) {
        PlanEstudio plan = PlanEstudio.findByAnio(anio)
        if(!plan)
            return false
        return estudiante.agregarCarrera(plan)
    }
}
