package guarani

import guarani.enums.Dia
import guarani.valueObject.RangoHorario

class BootStrap {

    private final static Long CODIGO_CARRERA_INGENIERIA_INFORMATICA = 10

    def init = { servletContext ->
 /*      Materia anaMatIIA = Materia.findByCodigo(6103)
                anaMatIIA.agregarCatedra('Troparevsky', [
                        [dia: Dia.Martes, rango: [inicio:9 , fin:13]],
                        [dia: Dia.Viernes, rango: [inicio:9 , fin:13]],
                    ]
                )

                anaMatIIA.agregarCatedra('Acero', [
                        [dia: Dia.Martes, rango: [inicio:7 , fin:11]],
                        [dia: Dia.Jueves, rango: [inicio:7 , fin:11]],
                    ]
                )

                anaMatIIA.agregarCatedra('Gigola', [
                        [dia: Dia.Miercoles, rango: [inicio:14 , fin:18]],
                        [dia: Dia.Viernes, rango: [inicio:14 , fin:18]],
                    ]
                )

                anaMatIIA.agregarCatedra('Zitto', [
                        [dia: Dia.Martes, rango: [inicio:9 , fin:13]],
                        [dia: Dia.Viernes, rango: [inicio:9 , fin:13]],
                    ]
                )

                anaMatIIA.agregarCatedra('Piotrkowski', [
                        [dia: Dia.Lunes, rango: [inicio:17 , fin:19]],
                        [dia: Dia.Miercoles, rango: [inicio:13 , fin:17]],
                    ]
                )

                anaMatIIA.agregarCatedra('Boggi', [
                        [dia: Dia.Lunes, rango: [inicio:9 , fin:13]],
                        [dia: Dia.Miercoles, rango: [inicio:9 , fin:13]],
                    ]
                )


                anaMatIIA.agregarCatedra('Unger', [
                        [dia: Dia.Martes, rango: [inicio:18 , fin:22]],
                        [dia: Dia.Viernes, rango: [inicio:18 , fin:22]],
                    ]
                )

                anaMatIIA.agregarCatedra('Vardanega', [
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:23]],
                        [dia: Dia.Sabado, rango: [inicio:9 , fin:13]],
                    ]
                )


                anaMatIIA.agregarCatedra('Sirne', [
                        [dia: Dia.Martes, rango: [inicio:14 , fin:18]],
                        [dia: Dia.Jueves, rango: [inicio:14 , fin:18]],
                    ]
                )

                anaMatIIA.agregarCatedra('Seminara', [
                        [dia: Dia.Lunes, rango: [inicio:17 , fin:21]],
                        [dia: Dia.Miercoles, rango: [inicio:17 , fin:21]],
                    ]
                )


        Materia fisicaIA = Materia.findByCodigo(6201)
                fisicaIA.agregarCatedra('Ferrini', [
                        [dia: Dia.Martes, rango: [inicio:8 , fin:12]],
                        [dia: Dia.Viernes, rango: [inicio:8 , fin:12]],
                    ]
                )


                fisicaIA.agregarCatedra('Garea', [
                        [dia: Dia.Lunes, rango: [inicio:8 , fin:12]],
                        [dia: Dia.Miercoles, rango: [inicio:8 , fin:12]],
                    ]
                )


                fisicaIA.agregarCatedra('Barasch', [
                        [dia: Dia.Martes, rango: [inicio:15 , fin:19]],
                        [dia: Dia.Jueves, rango: [inicio:15 , fin:19]],
                    ]
                )


                fisicaIA.agregarCatedra('Lombardi', [
                        [dia: Dia.Martes, rango: [inicio:15 , fin:19]],
                        [dia: Dia.Jueves, rango: [inicio:15 , fin:19]],
                    ]
                )


                fisicaIA.agregarCatedra('Moreno', [
                        [dia: Dia.Martes, rango: [inicio:19 , fin:23]],
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:23]],
                    ]
                )


                fisicaIA.agregarCatedra('Saccone', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:23]],
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:23]],
                    ]
                )


                fisicaIA.agregarCatedra('Echarri', [
                        [dia: Dia.Martes, rango: [inicio:19 , fin:23]],
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:23]],
                    ]
                )

                fisicaIA.agregarCatedra('Acosta', [
                        [dia: Dia.Miercoles, rango: [inicio:15 , fin:19]],
                        [dia: Dia.Viernes, rango: [inicio:15 , fin:19]],
                    ]
                )

                fisicaIA.agregarCatedra('Cornejo', [
                        [dia: Dia.Lunes, rango: [inicio:13 , fin:17]],
                        [dia: Dia.Jueves, rango: [inicio:13 , fin:17]],
                    ]
                )


                fisicaIA.agregarCatedra('Corsini', [
                        [dia: Dia.Lunes, rango: [inicio:8 , fin:12]],
                        [dia: Dia.Miercoles, rango: [inicio:8 , fin:12]],
                    ]
                )


        Materia algoYProI = Materia.findByCodigo(7540)
                algoYProI.agregarCatedra('Guarna', [
                        [dia: Dia.Martes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:22]],
                    ]
                )

                algoYProI.agregarCatedra('Azcurra', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
                    ]
                )


                algoYProI.agregarCatedra('Garcia', [
                        [dia: Dia.Lunes, rango: [inicio:8 , fin:11]],
                        [dia: Dia.Miercoles, rango: [inicio:7 , fin:10]],
                    ]
                )


                algoYProI.agregarCatedra('Wachenchauzer', [
                        [dia: Dia.Lunes, rango: [inicio:16 , fin:19]],
                        [dia: Dia.Viernes, rango: [inicio:16 , fin:19]],
                    ]
                )


                algoYProI.agregarCatedra('Servetto', [
                        [dia: Dia.Miercoles, rango: [inicio:16 , fin:19]],
                        [dia: Dia.Viernes, rango: [inicio:16 , fin:19]],
                    ]
                )


        Materia algoYProII = Materia.findByCodigo(7541)
                algoYProII.agregarCatedra('Carolo', [
                        [dia: Dia.Lunes, rango: [inicio:18 , fin:21]],
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:22]],
                    ]
                )

                algoYProII.agregarCatedra('Carolo', [
                        [dia: Dia.Lunes, rango: [inicio:18 , fin:21]],
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
                    ]
                )

                algoYProII.agregarCatedra('Calvo', [
                        [dia: Dia.Martes, rango: [inicio:18 , fin:22]],
                        [dia: Dia.Viernes, rango: [inicio:19 , fin:22]],
                    ]
                )


        Materia algebraIIA = Materia.findByCodigo(6108)
                algebraIIA.agregarCatedra('Vargas', [
                        [dia: Dia.Lunes, rango: [inicio:9 , fin:13]],
                        [dia: Dia.Miercoles, rango: [inicio:9 , fin:13]],
                    ]
                )


                algebraIIA.agregarCatedra('Cammilleri', [
                        [dia: Dia.Miercoles, rango: [inicio:9 , fin:13]],
                        [dia: Dia.Viernes, rango: [inicio:9 , fin:13]],
                    ]
                )

                algebraIIA.agregarCatedra('Pustilnik', [
                        [dia: Dia.Martes, rango: [inicio:9 , fin:13]],
                        [dia: Dia.Jueves, rango: [inicio:9 , fin:13]],
                    ]
                )


                algebraIIA.agregarCatedra('Martins', [
                        [dia: Dia.Martes, rango: [inicio:9 , fin:13]],
                        [dia: Dia.Jueves, rango: [inicio:9 , fin:13]],
                    ]
                )

                algebraIIA.agregarCatedra('Seinhart', [
                        [dia: Dia.Miercoles, rango: [inicio:13 , fin:17]],
                        [dia: Dia.Viernes, rango: [inicio:13 , fin:17]],
                    ]
                )


                algebraIIA.agregarCatedra('Alvarez', [
                        [dia: Dia.Lunes, rango: [inicio:17 , fin:21]],
                        [dia: Dia.Miercoles, rango: [inicio:17 , fin:21]],
                    ]
                )

        Materia fisicaIIA = Materia.findByCodigo(6203)
                fisicaIIA.agregarCatedra('Leone', [
                        [dia: Dia.Martes, rango: [inicio:8 , fin:13]],
                        [dia: Dia.Jueves, rango: [inicio:9 , fin:13]],
                    ]
                )

                fisicaIIA.agregarCatedra('Perez', [
                        [dia: Dia.Martes, rango: [inicio:9 , fin:14]],
                        [dia: Dia.Jueves, rango: [inicio:10 , fin:14]],
                    ]
                )

                fisicaIIA.agregarCatedra('Pivia', [
                        [dia: Dia.Martes, rango: [inicio:11 , fin:16]],
                        [dia: Dia.Jueves, rango: [inicio:11 , fin:16]],
                    ]
                )

                fisicaIIA.agregarCatedra('Ferrini', [
                        [dia: Dia.Martes, rango: [inicio:16 , fin:20]],
                        [dia: Dia.Jueves, rango: [inicio:16 , fin:20]],
                    ]
                )


                fisicaIIA.agregarCatedra('Fontana', [
                        [dia: Dia.Miercoles, rango: [inicio:7 , fin:13]],
                        [dia: Dia.Viernes, rango: [inicio:9 , fin:13]],
                    ]
                )

                fisicaIIA.agregarCatedra('Abraham', [
                        [dia: Dia.Miercoles, rango: [inicio:10 , fin:15]],
                        [dia: Dia.Viernes, rango: [inicio:11 , fin:15]],
                    ]
                )

                fisicaIIA.agregarCatedra('Margonari', [
                        [dia: Dia.Lunes, rango: [inicio:13 , fin:17]],
                        [dia: Dia.Miercoles, rango: [inicio:11 , fin:17]],
                    ]
                )

        Materia quimica = Materia.findByCodigo(6301)
                quimica.agregarCatedra('Grande', [
                        [dia: Dia.Martes, rango: [inicio:9 , fin:12]],
                        [dia: Dia.Jueves, rango: [inicio:9 , fin:12]],
                    ]
                )

                quimica.agregarCatedra('Ureña', [
                        [dia: Dia.Martes, rango: [inicio:9 , fin:12]],
                        [dia: Dia.Jueves, rango: [inicio:9 , fin:12]],
                    ]
                )

                quimica.agregarCatedra('Grande2', [
                        [dia: Dia.Martes, rango: [inicio:17 , fin:20]],
                        [dia: Dia.Jueves, rango: [inicio:17 , fin:20]],
                    ]
                )

                quimica.agregarCatedra('Ricotti', [
                        [dia: Dia.Sabado, rango: [inicio:7 , fin:13]],
                    ]
                )


        Materia fisicaIIID = Materia.findByCodigo(6215)
                fisicaIIID.agregarCatedra('Aguirre', [
                        [dia: Dia.Miercoles, rango: [inicio:18 , fin:20]],
                        [dia: Dia.Jueves, rango: [inicio:18 , fin:20]],
                    ]
                )


                fisicaIIID.agregarCatedra('Arcondo', [
                        [dia: Dia.Miercoles, rango: [inicio:16 , fin:18]],
                        [dia: Dia.Jueves, rango: [inicio:18 , fin:20]],
                    ]
                )

        Materia laboratorio = Materia.findByCodigo(6602)
                laboratorio.agregarCatedra('Marino', [
                        [dia: Dia.Lunes, rango: [inicio:16 , fin:22]],
                    ]
                )

                laboratorio.agregarCatedra('Florentin', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Miercoles, rango: [inicio:15 , fin:18]],
                    ]
                )

                laboratorio.agregarCatedra('Nicora', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
                    ]
                )


                laboratorio.agregarCatedra('Cervetto', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Viernes, rango: [inicio:19 , fin:22]],
                    ]
                )


        Materia estrucComp = Materia.findByCodigo(6670)
                estrucComp.agregarCatedra('Cabibbo', [
                        [dia: Dia.Martes, rango: [inicio:16 , fin:19]],
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
                    ]
                )

                estrucComp.agregarCatedra('Trinchero', [
                        [dia: Dia.Martes, rango: [inicio:16 , fin:19]],
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:22]],
                    ]
                )


                estrucComp.agregarCatedra('Burin', [
                        [dia: Dia.Martes, rango: [inicio:16 , fin:19]],
                        [dia: Dia.Viernes, rango: [inicio:16 , fin:19]],
                    ]
                )


        Materia algoYProgII = Materia.findByCodigo(7507)
                algoYProgII.agregarCatedra('Suarez', [
                        [dia: Dia.Lunes, rango: [inicio:16 , fin:19]],
                        [dia: Dia.Jueves, rango: [inicio:16 , fin:19]],
                    ]
                )


                algoYProgII.agregarCatedra('Paez', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:22]],
                    ]
                )

        Materia analisisNumeI = Materia.findByCodigo(7512)
                analisisNumeI.agregarCatedra('Menendez', [
                        [dia: Dia.Lunes, rango: [inicio:16 , fin:19]],
                        [dia: Dia.Jueves, rango: [inicio:16 , fin:19]],
                    ]
                )

                analisisNumeI.agregarCatedra('Tarela', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
                    ]
                )

                analisisNumeI.agregarCatedra('Cavaliere', [
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Viernes, rango: [inicio:19 , fin:22]],
                    ]
                )

                analisisNumeI.agregarCatedra('Gomez', [
                        [dia: Dia.Martes, rango: [inicio:8 , fin:11]],
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:22]],
                    ]
                )

                analisisNumeI.agregarCatedra('Griggio', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
                    ]
                )

                analisisNumeI.agregarCatedra('Schwarz', [
                        [dia: Dia.Lunes, rango: [inicio:16 , fin:19]],
                        [dia: Dia.Viernes, rango: [inicio:16 , fin:19]],
                    ]
                )

                analisisNumeI.agregarCatedra('Sassano', [
                        [dia: Dia.Miercoles, rango: [inicio:14 , fin:17]],
                        [dia: Dia.Jueves, rango: [inicio:14 , fin:17]],
                    ]
                )

                analisisNumeI.agregarCatedra('Rodriguez', [
                        [dia: Dia.Miercoles, rango: [inicio:14 , fin:17]],
                        [dia: Dia.Jueves, rango: [inicio:14 , fin:17]],
                    ]
                )


        Materia probaB = Materia.findByCodigo(6109)
                probaB.agregarCatedra('Gil', [
                        [dia: Dia.Martes, rango: [inicio:12 , fin:15]],
                        [dia: Dia.Miercoles, rango: [inicio:12 , fin:15]],
                    ]
                )

                probaB.agregarCatedra('Busch', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:21]],
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:23]],
                    ]
                )


                probaB.agregarCatedra('Gil2', [
                        [dia: Dia.Martes, rango: [inicio:9 , fin:12]],
                        [dia: Dia.Miercoles, rango: [inicio:9 , fin:12]],
                    ]
                )


                probaB.agregarCatedra('Perez', [
                        [dia: Dia.Lunes, rango: [inicio:15 , fin:17]],
                        [dia: Dia.Miercoles, rango: [inicio:15 , fin:19]],
                    ]
                )

        Materia anaMatIIIA = Materia.findByCodigo(6110)
                anaMatIIIA.agregarCatedra('Acero', [
                        [dia: Dia.Martes, rango: [inicio:9 , fin:13]],
                        [dia: Dia.Jueves, rango: [inicio:9 , fin:13]],
                    ]
                )


                anaMatIIIA.agregarCatedra('Prelat', [
                        [dia: Dia.Martes, rango: [inicio:9 , fin:13]],
                        [dia: Dia.Viernes, rango: [inicio:9 , fin:13]],
                    ]
                )

                anaMatIIIA.agregarCatedra('Murmis', [
                        [dia: Dia.Martes, rango: [inicio:16 , fin:20]],
                        [dia: Dia.Jueves, rango: [inicio:16 , fin:20]],
                    ]
                )

                anaMatIIIA.agregarCatedra('Maestripieri', [
                        [dia: Dia.Martes, rango: [inicio:16 , fin:20]],
                        [dia: Dia.Jueves, rango: [inicio:16 , fin:20]],
                    ]
                )

                anaMatIIIA.agregarCatedra('Cachile', [
                        [dia: Dia.Martes, rango: [inicio:14 , fin:18]],
                        [dia: Dia.Jueves, rango: [inicio:14 , fin:18]],
                    ]
                )

        Materia estruComp = Materia.findByCodigo(6620)
                estruComp.agregarCatedra('Hamkalo', [
                        [dia: Dia.Lunes, rango: [inicio:16 , fin:19]],
                        [dia: Dia.Martes, rango: [inicio:19 , fin:22]],
                    ]
                )

                estruComp.agregarCatedra('Hamkalo2', [
                        [dia: Dia.Lunes, rango: [inicio:16 , fin:19]],
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:22]],
                    ]
                )


        Materia datos = Materia.findByCodigo(7506)
                datos.agregarCatedra('Argerich', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:22]],
                    ]
                )


                datos.agregarCatedra('Servetto', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
                    ]
                )

        Materia tallerI = Materia.findByCodigo(7542)
                tallerI.agregarCatedra('Veiga', [
                        [dia: Dia.Martes, rango: [inicio:18 , fin:22]],
                    ]
                )

                tallerI.agregarCatedra('Azcurra', [
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:23]],
                    ]
                )

        Materia estrOrga = Materia.findByCodigo(7112)
                estrOrga.agregarCatedra('Reyes', [
                        [dia: Dia.Miercoles, rango: [inicio:17 , fin:23]],
                    ]
                )

        Materia modelosI = Materia.findByCodigo(7114)
                modelosI.agregarCatedra('Ramos-Navarro', [
                        [dia: Dia.Martes, rango: [inicio:18 , fin:21]],
                        [dia: Dia.Miercoles, rango: [inicio:18 , fin:21]],
                    ]
                )

                modelosI.agregarCatedra('Ramonet-Navarro', [
                        [dia: Dia.Martes, rango: [inicio:18 , fin:21]],
                        [dia: Dia.Jueves, rango: [inicio:9 , fin:12]],
                    ]
                )

                modelosI.agregarCatedra('Ramos-Oitana', [
                        [dia: Dia.Miercoles, rango: [inicio:18 , fin:21]],
                        [dia: Dia.Viernes, rango: [inicio:19 , fin:22]],
                    ]
                )

                modelosI.agregarCatedra('Ramonet-Oitana', [
                        [dia: Dia.Jueves, rango: [inicio:9 , fin:12]],
                        [dia: Dia.Viernes, rango: [inicio:19 , fin:22]],
                    ]
                )

                modelosI.agregarCatedra('Ramos-Echeverria', [
                        [dia: Dia.Martes, rango: [inicio:9 , fin:12]],
                        [dia: Dia.Miercoles, rango: [inicio:18 , fin:21]],
                    ]
                )

                modelosI.agregarCatedra('Ramonet-Echeverria', [
                        [dia: Dia.Martes, rango: [inicio:9 , fin:12]],
                        [dia: Dia.Jueves, rango: [inicio:9 , fin:12]],
                    ]
                )

        Materia sisOp = Materia.findByCodigo(7508)
                sisOp.agregarCatedra('Clua', [
                        [dia: Dia.Martes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:22]],
                    ]
                )


        Materia anaInfor = Materia.findByCodigo(7509)
                anaInfor.agregarCatedra('Turri', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
                    ]
                )

                anaInfor.agregarCatedra('Rivaduilla', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Viernes, rango: [inicio:19 , fin:22]],
                    ]
                )

                anaInfor.agregarCatedra('Villagra', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:22]],
                    ]
                )


        Materia tecDis = Materia.findByCodigo(7510)
                tecDis.agregarCatedra('Pantaleo', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:22]],
                    ]
                )


        Materia baseDatos = Materia.findByCodigo(7515)
                baseDatos.agregarCatedra('Ale', [
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
                    ]
                )

        Materia introDistrib = Materia.findByCodigo(7543)
                introDistrib.agregarCatedra('Bernardez', [
                        [dia: Dia.Martes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Viernes, rango: [inicio:19 , fin:22]],
                    ]
                )

        Materia tallerII = Materia.findByCodigo(7552)
                tallerII.agregarCatedra('Veiga', [
                        [dia: Dia.Jueves, rango: [inicio:15 , fin:19]],
                    ]
                )


        Materia inforOrga = Materia.findByCodigo(7113)
                inforOrga.agregarCatedra('Markdorf', [
                        [dia: Dia.Martes, rango: [inicio:17 , fin:23]],
                    ]
                )

        Materia admYContProyI = Materia.findByCodigo(7544)
                admYContProyI.agregarCatedra('Fontela', [
                        [dia: Dia.Martes, rango: [inicio:16 , fin:22]],
                    ]
                )


        Materia tallerProyI = Materia.findByCodigo(7545)
                tallerProyI.agregarCatedra('Pignataro', [
                        [dia: Dia.Jueves, rango: [inicio:17 , fin:23]],
                    ]
                )

        Materia admYContProyII = Materia.findByCodigo(7546)
                admYContProyII.agregarCatedra('Martinez', [
                        [dia: Dia.Jueves, rango: [inicio:16 , fin:22]],
                    ]
                )


        Materia admYContProyIII = Materia.findByCodigo(7547)
                admYContProyIII.agregarCatedra('Fontela', [
                        [dia: Dia.Lunes, rango: [inicio:17 , fin:23]],
                    ]
                )

        Materia calidadDesSis = Materia.findByCodigo(7548)
                calidadDesSis.agregarCatedra('Pantaleo', [
                        [dia: Dia.Miercoles, rango: [inicio:18 , fin:22]],
                    ]
                )

        Materia legYEjProfInf = Materia.findByCodigo(7140)
                legYEjProfInf.agregarCatedra('Noremberg', [
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:23]],
                    ]
                )


        Materia anaCirc = Materia.findByCodigo(6606)
                anaCirc.agregarCatedra('Contigiani', [
                        [dia: Dia.Martes, rango: [inicio:8 , fin:11]],
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Jueves, rango: [inicio:8 , fin:12]],
                    ]
                )


                anaCirc.agregarCatedra('Barreiro', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:23]],
                    ]
                )


        Materia concurreI = Materia.findByCodigo(7559)
                concurreI.agregarCatedra('Echeverria', [
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Sabado, rango: [inicio:9 , fin:12]],
                    ]
                )

        Materia seYSis = Materia.findByCodigo(6674)
                seYSis.agregarCatedra('Azcueta', [
                        [dia: Dia.Lunes, rango: [inicio:16 , fin:19]],
                        [dia: Dia.Martes, rango: [inicio:16 , fin:19]],
                    ]
                )


                seYSis.agregarCatedra('Casaglia', [
                        [dia: Dia.Lunes, rango: [inicio:16 , fin:19]],
                        [dia: Dia.Miercoles, rango: [inicio:16 , fin:19]],
                    ]
                )

                seYSis.agregarCatedra('Torres', [
                        [dia: Dia.Lunes, rango: [inicio:16 , fin:19]],
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
                    ]
                )

                seYSis.agregarCatedra('Scaramal', [
                        [dia: Dia.Lunes, rango: [inicio:16 , fin:19]],
                        [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
                    ]
                )

        Materia sisDistI = Materia.findByCodigo(7574)
        sisDistI.agregarCatedra('Menendez', [
                        [dia: Dia.Lunes, rango: [inicio:18 , fin:21]],
                        [dia: Dia.Martes, rango: [inicio:18 , fin:21]],
                    ]
                )

        Materia tallerIII = Materia.findByCodigo(7561)
        tallerIII.agregarCatedra('Veiga', [
                        [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                        [dia: Dia.Jueves, rango: [inicio:19 , fin:22]],
                    ]
                )

        //alta estudiantes
        Estudiante phazan = Estudiante.crearEstudiante('Pablo', 'Hazan', 96522, 'phazan', '123')
        Estudiante jcasal = Estudiante.crearEstudiante('Joaquin', 'Casal', 98280, 'jcasal', '123')
        Estudiante mfernandezvidal = Estudiante.crearEstudiante('Mariano', 'Fernandez Vidal', 89789, 'mfernandezvidal', '123')

        //alta catedras
        Materia algoI = Materia.findByCodigo(7540)
        algoI.agregarCatedra('Lague',[
                [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
        ])

        Materia algoII = Materia.findByCodigo(7541)
        algoII.agregarCatedra('Wachenchauzer',[
                [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
        ])

        Materia algoIII = Materia.findByCodigo(7507)
        algoIII.agregarCatedra('Fontela', [
                [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
        ]
        )

        Materia discreta = Materia.findByCodigo(6107)
        discreta.agregarCatedra('Acero', [
                [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
        ]
        )
        discreta.agregarCatedra('Lorruso', [
                [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
        ]
        )
        discreta.agregarCatedra('Perez', [
                [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
        ]
        )

        Materia algebraII = Materia.findByCodigo(6108)
        algebraII.agregarCatedra('Acero', [
                [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
        ]
        )

        Materia aninfo = Materia.findByCodigo(7509)
        aninfo.agregarCatedra('Gonzalez', [
                [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
        ]
        )

        Materia analisisIII = Materia.findByCodigo(6110)
        analisisIII.agregarCatedra('Gonzalez', [
                [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
        ]
        )

        baseDatos.agregarCatedra('Servetto', [
                [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
        ]
        )

        Materia proba = Materia.findByCodigo(6109)
        proba.agregarCatedra('Martinez', [
                [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
        ]
        )

        Materia tdl = Materia.findByCodigo(7531)
        tdl.agregarCatedra('Wachenchauzer', [
                [dia: Dia.Lunes, rango: [inicio:18 , fin:22]],
            ]
        )


        Materia tda = Materia.findByCodigo(7529)
        tda.agregarCatedra('Wachenchauzer', [
                [dia: Dia.Lunes, rango: [inicio:20 , fin:22]],
                [dia: Dia.Viernes, rango: [inicio:20 , fin:22]],
            ]
        )

        Materia fisicaII = Materia.findByCodigo(6203)
        fisicaII.agregarCatedra('Sirkin', [
                [dia: Dia.Lunes, rango: [inicio:20 , fin:22]],
                [dia: Dia.Viernes, rango: [inicio:20 , fin:22]],
        ]
        )
        fisicaII.agregarCatedra('Mastronardi', [
                [dia: Dia.Lunes, rango: [inicio:20 , fin:22]],
                [dia: Dia.Viernes, rango: [inicio:20 , fin:22]],
        ]
        )

        quimica.agregarCatedra('Garcia', [
                [dia: Dia.Lunes, rango: [inicio:20 , fin:22]],
                [dia: Dia.Viernes, rango: [inicio:20 , fin:22]],
        ]
        )

        Materia estructura = Materia.findByCodigo(6670)
        estructura.agregarCatedra('Mazzeo', [
                [dia: Dia.Lunes, rango: [inicio:20 , fin:22]],
                [dia: Dia.Viernes, rango: [inicio:20 , fin:22]],
        ]
        )

        Materia numerico = Materia.findByCodigo(7512)
        numerico.agregarCatedra('Cavalieri', [
                [dia: Dia.Lunes, rango: [inicio:20 , fin:22]],
                [dia: Dia.Viernes, rango: [inicio:20 , fin:22]],
        ]
        )

        modelosI.agregarCatedra('Ramos', [
                [dia: Dia.Lunes, rango: [inicio:20 , fin:22]],
                [dia: Dia.Viernes, rango: [inicio:20 , fin:22]],
        ]
        )

        Materia simulacion = Materia.findByCodigo(7526)
        simulacion.agregarCatedra('Cadoche', [
                [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
        ]
        )

        Materia sistemasGraficos = Materia.findByCodigo(6671)
        sistemasGraficos.agregarCatedra('Abbate', [
                [dia: Dia.Lunes, rango: [inicio:19 , fin:22]],
                [dia: Dia.Miercoles, rango: [inicio:19 , fin:22]],
        ]
        )

        //Anotar estudiantes a carreras
        Carrera ingenieriaInformatica = Carrera.findByCodigo(CODIGO_CARRERA_INGENIERIA_INFORMATICA)
        ingenieriaInformatica.anotarEstudiante(phazan, 1986)
        ingenieriaInformatica.anotarEstudiante(mfernandezvidal, 1986)
        ingenieriaInformatica.anotarEstudiante(jcasal, 1986)

        //agregar materias finalizadas a estudiantes
        phazan.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Azcurra', algoI, 9)
        phazan.agregarOpinionCatedra('Azcurra', algoI, 1, "esta piola")
        phazan.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Pietrokovsky', anaMatIIA, 9)
        phazan.agregarOpinionCatedra('Pietrokovsky', anaMatIIA, 2, "esta piola")
        phazan.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Carolo', algoII, 9)
        phazan.agregarOpinionCatedra('Carolo', algoII, 3, "esta piola")
        phazan.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Perez', discreta, 9)
        phazan.agregarOpinionCatedra('Perez', discreta, 3, "esta piola")
        phazan.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Fontela', algoIII, 9)
        phazan.agregarOpinionCatedra('Fontela', algoIII, 4, "esta piola")
        phazan.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Garea', fisicaIA, 9)
        phazan.agregarOpinionCatedra('Garea', fisicaIA, 4, "esta piola")
        phazan.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Wachenchauzer', tdl, 9)
        phazan.agregarOpinionCatedra('Wachenchauzer', tdl, 5, "esta piola")
        phazan.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Vargas', algebraII, 9)
        phazan.agregarOpinionCatedra('Vargas', algebraII, 4, "esta piola")
        phazan.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Mastronardi', fisicaII, 9)
        phazan.agregarOpinionCatedra('Mastronardi', fisicaII, 3, "esta piola")
        phazan.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Mazzeo', estructura, 9)
        phazan.agregarOpinionCatedra('Mazzeo', estructura, 2, "esta piola")
        phazan.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Cavalieri', numerico, 9)
        phazan.agregarOpinionCatedra('Cavalieri', numerico, 3, "esta piola")
        phazan.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Veiga', tallerI, 9)
        phazan.agregarOpinionCatedra('Veiga', tallerI, 5, "esta piola")
        phazan.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Reyes', estrOrga, 9)
        phazan.agregarOpinionCatedra('Reyes', estrOrga, 1, "esta piola")

        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Sirne', anaMatIIA, 9)
        mfernandezvidal.agregarOpinionCatedra('Sirne', anaMatIIA, 3, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Lague', algoI, 9)
        mfernandezvidal.agregarOpinionCatedra('Lague', algoI, 4, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Lorruso', discreta, 9)
        mfernandezvidal.agregarOpinionCatedra('Lorruso', discreta, 4, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Acero', algebraII, 9)
        mfernandezvidal.agregarOpinionCatedra('Acero', algebraII, 3, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Carolo', algoII, 9)
        mfernandezvidal.agregarOpinionCatedra('Carolo', algoII, 1, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Carolo', algoII, 9)
        mfernandezvidal.agregarOpinionCatedra('Carolo', algoII, 1, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Garcia', quimica, 9)
        mfernandezvidal.agregarOpinionCatedra('Garcia', quimica, 3, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Sirkin', fisicaII, 9)
        mfernandezvidal.agregarOpinionCatedra('Sirkin', fisicaII, 2, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Calvo', algoII, 9)
        mfernandezvidal.agregarOpinionCatedra('Calvo', algoII, 4, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Arcondo', fisicaIIID, 9)
        mfernandezvidal.agregarOpinionCatedra('Arcondo', fisicaIIID, 2, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Mazzeo', estructura, 9)
        mfernandezvidal.agregarOpinionCatedra('Mazzeo', estructura, 2, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Tarela', numerico, 9)
        mfernandezvidal.agregarOpinionCatedra('Tarela', numerico, 3, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Gonzalez', analisisIII, 9)
        mfernandezvidal.agregarOpinionCatedra('Gonzalez', analisisIII, 3, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Perez', proba, 9)
        mfernandezvidal.agregarOpinionCatedra('Perez', proba, 4, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Marino', laboratorio, 9)
        mfernandezvidal.agregarOpinionCatedra('Marino', laboratorio, 3, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Fontela', algoIII, 9)
        mfernandezvidal.agregarOpinionCatedra('Fontela', algoIII, 4, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Azcurra', tallerI, 9)
        mfernandezvidal.agregarOpinionCatedra('Azcurra', tallerI, 3, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Argerich', datos, 9)
        mfernandezvidal.agregarOpinionCatedra('Argerich', datos, 5, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Gonzalez', aninfo, 9)
        mfernandezvidal.agregarOpinionCatedra('Gonzalez', aninfo, 3, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Reyes', estrOrga, 9)
        mfernandezvidal.agregarOpinionCatedra('Reyes', estrOrga, 1, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Ramos', modelosI, 9)
        mfernandezvidal.agregarOpinionCatedra('Ramos', modelosI, 4, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Ale', baseDatos, 9)
        mfernandezvidal.agregarOpinionCatedra('Ale', baseDatos, 3, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Clua', sisOp, 9)
        mfernandezvidal.agregarOpinionCatedra('Clua', sisOp, 3, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Hamkalo', estrucComp, 9)
        mfernandezvidal.agregarOpinionCatedra('Hamkalo', estrucComp, 2, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Veiga', tallerII, 9)
        mfernandezvidal.agregarOpinionCatedra('Veiga', tallerII, 3, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Bernardez', introDistrib, 9)
        mfernandezvidal.agregarOpinionCatedra('Bernardez', introDistrib, 4, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Cadoche', simulacion, 9)
        mfernandezvidal.agregarOpinionCatedra('Cadoche', simulacion, 2, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Abbate', sistemasGraficos, 9)
        mfernandezvidal.agregarOpinionCatedra('Abbate', sistemasGraficos, 5, "no me gusto")
        mfernandezvidal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Echeverria', concurreI, 9)
        mfernandezvidal.agregarOpinionCatedra('Echeverria', concurreI, 4, "no me gusto")

        jcasal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Wachenchauzer', algoI, 9)
        jcasal.agregarOpinionCatedra('Wachenchauzer', algoI, 5, "_")
        jcasal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Wachenchauzer', algoII, 9)
        jcasal.agregarOpinionCatedra('Wachenchauzer', algoII, 5, "_")
        jcasal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Acero', discreta, 9)
        jcasal.agregarOpinionCatedra('Acero', discreta, 3, "_")
        jcasal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Fontela', algoIII, 9)
        jcasal.agregarOpinionCatedra('Fontela', algoIII, 4, "_")
        jcasal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Vargas', algebraII, 9)
        jcasal.agregarOpinionCatedra('Vargas', algebraII, 2, "_")
        jcasal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Argerich', datos, 9)
        jcasal.agregarOpinionCatedra('Argerich', datos, 1, "muy mala")
        jcasal.agregarOpinionCatedra('Argerich', datos, 5, "_")
        jcasal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Reyes', estrOrga, 9)
        jcasal.agregarOpinionCatedra('Reyes', estrOrga, 1, "_")
        jcasal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Villagra', aninfo, 9)
        jcasal.agregarOpinionCatedra('Villagra', aninfo, 4, "_")
        jcasal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Seminara', anaMatIIA, 9)
        jcasal.agregarOpinionCatedra('Seminara', anaMatIIA, 1, "_")
        jcasal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Clua', sisOp, 9)
        jcasal.agregarOpinionCatedra('Clua', sisOp, 2, "_")
        jcasal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Servetto', baseDatos, 9)
        jcasal.agregarOpinionCatedra('Servetto', baseDatos, 2, "_")
        jcasal.agregarMateriaAprobada(CODIGO_CARRERA_INGENIERIA_INFORMATICA, 'Martinez', proba, 9)
        jcasal.agregarOpinionCatedra('Martinez', proba, 2, "_")

        //cargar intereses
        List<String> iteresesJcasalStr = ['Programacion funcional','Machine Learning','Estructura de datos','Grafos','Software embebido','Criptografia','Compresion','Blockchain','Ingenieria de software','Sistemas de recomendacion','C','C++','Ruby','Rails','JavaScript','Python','R','Git','BDD','TensorFlow']
        Set<Interes> interesesJcasal = Interes.findAllByTagInList(iteresesJcasalStr)
        jcasal.agregarIntereses(interesesJcasal)

        List<String> interesesMfernandezvidalStr = ['Metodologias agiles', 'Seguridad informatica', 'Algoritmos cuanticos', 'IntelliJ', 'Sublime', 'MySQL', 'SQLitte', 'MongoDB', 'Github', 'Git', 'Integracion continua', 'Ubuntu', 'Unix', 'Python', 'Docker', 'JavaScript', 'Grails', 'Groovy', 'Java', 'Android', 'Sistemas de recomendación', 'Protocolos de seguridad', 'Sistemas distribuidos', 'NoSQL', 'Base de datos', 'SQL', 'Concurrencia', 'Sockets', 'Criptografia', 'Compresion', 'Arduino', 'Probabilidad', 'Cuantica', 'POO', 'Programacion funcional']
        Set<String> interesesMfernandezvidal = Interes.findAllByTagInList(interesesMfernandezvidalStr)
        mfernandezvidal.agregarIntereses(interesesMfernandezvidal)

       def rangosDeHorarios = (7..22).collect { hora ->
           new RangoHorario(hora,hora+1)
       }

       (Dia.Lunes..Dia.Sabado).collect { dia ->
           rangosDeHorarios.collect { rango ->
               new Horario(dia: dia, rango: rango, horarioDeCatedra: false)
           }
       }.flatten().collect {Horario h -> h.save(flush:true, failOnError: true)}
*/
    }
    
    def destroy = {
    }
}
