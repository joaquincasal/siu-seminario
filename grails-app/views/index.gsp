<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Guarani</title>
</head>
<body>

    <div id="content" role="main">
        <section class="row colset-2-its">
            <h1>SIU Guarani</h1>

            <p>
                Sistema de inscripciones
            </p>

            <div class="row">
                <div class="col-md-12">
                    <g:link controller="estudiante" name="estudiantes" class="btn btn-primary">
                        Alta de estudiantes
                    </g:link>

                    <g:link controller="simuladorCuatrimestre" name="simuladorCuatrimestre" class="btn btn-primary">
                        Simulador de cuatrimestre
                    </g:link>

                    <g:link controller="organizadorDeHorarios" name="organizardorHorarios" class="btn btn-primary">
                        Organizador de horarios
                    </g:link>

                    <g:link controller="recomendadorMaterias" name="recomendator" class="btn btn-primary">
                        Intereses, opiniones y recomendaciones
                    </g:link>

                </div>
            </div>

        </section>
    </div>

</body>
</html>
