<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Seleccionar plan de estudio</title>
</head>

<body>
<a href="#elegir-plan-estudio" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                     default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
    </ul>
</div>

<div id="elegir-plan-estudio" class="content scaffold-create" role="main">
    <h1>Simulador de cuatimestre</h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:form action="simular" method="POST">
        <div class="row">
            <div class="col-md-12">
                <g:submitButton name="create" class="btn btn-primary" value="Seleccionar"/>
            </div>
        </div>
        <g:hiddenField name="planEstudioId" value="${planEstudioId}"/>
        <div class="row">
            <div class="col-md-4">
                <h2>Aprobadas</h2>
                <g:each in="${materiasAprobadas}" var="materia">
                    <li>
                        <g:if test="${seleccionadasAntes?.any{it == materia.id}}">+</g:if>
                        ${materia.nombre}
                    </li>
                    <g:hiddenField name="materiasAprobadasIds" value="${materia.id}"/>
                </g:each>
            </div>

            <div class="col-md-4">
                <h2>Habilitadas</h2>
                <g:each in="${materiasHabilitadas}" var="materia">
                    <li><g:checkBox name="materiasSeleccionadasCb${materia.id}"
                                    id="materiasSeleccionadasCb${materia.id}"
                                    onclick="clickCheckbox('materiasSeleccionadasCb${materia.id}', ${materia.id})"/>
                    <g:if test="${materiasNuevas?.any{it.id == materia.id }}">+</g:if>


                        ${materia.nombre}
                    </li>
                </g:each>
                <g:hiddenField name="materiasSeleccionadasIds" value="" id="materiasSeleccionadasIds"/>
            </div>

            <div class="col-md-4">
                <h2>No habilitadas</h2>
                <g:each in="${materiasRestantes}" var="materia">
                    <li>${materia.nombre}</li>
                </g:each>
            </div>
        </div>
    </g:form>

    <script type="text/javascript">
        function clickCheckbox(chId, materiaId) {
            console.log(chId)
            var isChecked = $("#" + chId)[0].checked;
            var idsMateriasHf = $('#materiasSeleccionadasIds');
            var idsMaterias = $('#materiasSeleccionadasIds').val();
            if (isChecked) {
                idsMateriasHf.val(idsMaterias + materiaId +' ,' );
            } else {
                var newValue = $('#materiasSeleccionadasIds').val().replace(materiaId + ' ,', '');
                idsMateriasHf.val(newValue);
            }
        }
    </script>
</div>
</body>
</html>