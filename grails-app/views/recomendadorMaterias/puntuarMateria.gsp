<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>Recomendador de materias</title>
</head>
<body>
<a href="#create-estudiante" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="misOpinionesEIntereses">Mis opiniones</g:link></li>
    </ul>
</div>
<div id="create-estudiante" class="content scaffold-create" role="main">
    <h1>Puntuar materia</h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:form action="guardarPuntaje" method="POST">
        <div class="row">
            <div class="col-md-4">
                <label for="catedra">Catedra</label>
                <br/>
                <g:select id="catedra" name="idCatedra" from="${catedras}"
                    style="width:100%"
                    noSelection="['':'Elegir catedra']"
                    optionValue="fullName"
                    optionKey="id"
                />
            </div>
            <div class="col-md-4">
                <label for="puntaje">Puntaje</label>
                <br/>
                <g:field id="puntaje" type="number" name="puntaje" min="1" max="5" style="width:100%"/>
            </div>
            <div class="col-md-4">
                <label for="comentario">Comentario</label>
                <br/>
                <g:textArea id="comentario" name="comentario" style="width:100%"/>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                <g:submitButton name="create" class="btn btn-primary" value="Opinar" />
            </div>
        </div>
    </g:form>
</div>
</body>
</html>
