<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>Recomendador de materias</title>
</head>
<body>
<a href="#create-estudiante" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="misOpinionesEIntereses">Mis opiniones</g:link></li>
    </ul>
</div>
<div id="create-estudiante" class="content scaffold-create" role="main">
    <h1>Intereses</h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:form action="guardarIntereses" method="POST">
        <div class="row">
            <div class="col-md-12">
                <select class="autocompleteIntereses" name="interesesId" style="width:100%" multiple="multiple">
                    <g:each in="${intereses}" var="interes">
                        <option value="${interes.id}">${interes.tag}</option>
                    </g:each>
                </select>
            </div>            
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                <g:submitButton name="create" class="btn btn-primary" value="Agregar" />
            </div>
        </div>
    </g:form>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".autocompleteIntereses").select2({});
        });

    </script>
</div>
</body>
</html>
