<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>Recomendador de materias</title>
</head>
<body>
<a href="#create-estudiante" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="misOpinionesEIntereses">Mis opiniones</g:link></li>
    </ul>
</div>
<div id="create-estudiante" class="content scaffold-create" role="main">
    <h1>Mis opiniones</h1>
    <g:if test="${flash.message}">
        <br/>
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <table>
                <thead>
                <tr>
                    <th>Puntaje</th>
                    <th>Materia</th>
                    <th>Catedra</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${recomendaciones.sort().reverse()}" var="recomendacion">
                    <tr>
                        <th>${recomendacion.puntaje}</th>
                        <th>${recomendacion.catedra.materia.nombre}</th>
                        <th>${recomendacion.catedra.titular}</th>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
