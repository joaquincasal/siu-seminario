<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>Recomendador de materias</title>
</head>
<body>
<a href="#create-estudiante" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="misOpiniones">Mis opiniones</g:link></li>
    </ul>
</div>
<div id="create-estudiante" class="content scaffold-create" role="main">
    <h1>Mis opiniones</h1>
    <g:if test="${flash.message}">
        <br/>
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <g:link action="puntuarMateria" name="nuevaOpinion" class="btn btn-primary">
                Opinar
            </g:link>
            <g:link action="elegirIntereses" name="nuevoInteres" class="btn btn-primary">
                Agregar interes
            </g:link>
            <g:link action="recomendarmeMaterias" class="btn btn-primary">
                Recomendaciones
            </g:link>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-10">
            <table class="table">
                <thead>
                <tr>
                    <th>Materia</th>
                    <th>Catedra</th>
                    <th>Puntaje</th>
                    <th>Comentario</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${misOpiniones.sort {a, b -> a.id - b.id}}" var="opinion">
                    <tr>
                        <th>${opinion.catedra.materia.nombre}</th>
                        <th>${opinion.catedra.titular}</th>
                        <th>${opinion.puntaje}</th>
                        <th>${opinion.comentario}</th>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
        <div class="col-md-2">
            <table class="table">
                <thead>
                <tr>
                    <th>Interes</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${misIntereses.sort {a, b -> a.id - b.id}}" var="interes">
                    <tr>
                        <th>${interes.tag}</th>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
