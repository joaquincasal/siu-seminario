<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>Recomendador de materias</title>
</head>
<body>
<a href="#create-estudiante" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="elegirPlanEstudio">Elegir plan de estudio</g:link></li>
    </ul>
</div>
<div id="create-estudiante" class="content scaffold-create" role="main">
    <h1>Recomendaciones de horarios</h1>
    <g:if test="${flash.message}">
        <br/>
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <div class="row">
        <div class="col-md-12">
            <g:if test="${catedras.size() != 0}">
            <table class="table">
                <thead>
                <tr>
                    <th>Recomendación</th>
                    <th>Materia</th>
                    <th>Catedra</th>
                    <th>Horario</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${catedras}" var="c" status="i">
                    <g:each in="${c}" var="catedra">
                        <tr>
                        <th>${i}</th>
                        <th>${catedra.materia.nombre}</th>
                        <th>${catedra.titular}</th>
                        <th>
                            <g:each in="${catedra.horarios.sort()}" var="horario">
                                <li>
                                    ${horario.dia.id} de ${horario.rango.inicio} a ${horario.rango.fin}
                                </li>
                            </g:each>
                        </th>
                        </tr>
                    </g:each>
                </g:each>

                </tbody>
            </table>
            </g:if>
            <g:else>
                <h2>No se encontraron combinaciones posibles</h2>
            </g:else>
        </div>
    </div>
</div>
</body>
</html>