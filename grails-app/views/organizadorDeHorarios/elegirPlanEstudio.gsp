<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>Seleccionar plan de estudio</title>
</head>
<body>
<a href="#elegir-plan-estudio" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
    </ul>
</div>
<div id="elegir-plan-estudio" class="content scaffold-create" role="main">
    <h1>Planes de estudio</h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:form action="${ actionReturn ?: 'elegirHorarios'}" controller="${simuladorCuatrimestre}" method="POST">
        <div class="row">
            <div class="col-md-12">
                <g:select name="planEstudioId"
                          from="${planesEstudio}"
                          optionValue="nombre"
                          optionKey="id"
                />
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                <g:submitButton name="create" class="btn btn-primary" value="Seleccionar" />
            </div>
        </div>
    </g:form>
</div>
</body>
</html>
