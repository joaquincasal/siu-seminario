<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <title>Recomendador de materias</title>



</head>
<body>

<a href="#elegir-horarios" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="elegirPlanEstudio">Mis planes de estudio</g:link></li>
    </ul>
</div>
<div id="elegir-horarios" class="content scaffold-create" role="main">
    <h1>Materias</h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:form action="recomendarHorarios" method="POST">
        <g:hiddenField name="planEstudioId" value="${planEstudioId}"/>
        <div class="row">
            <div class="col-md-12">
                <select class="autocompleteSelect2" name="idMaterias" style="width:100%" multiple="multiple">
                    <g:each in="${materias}" var="materia">
                        <option value="${materia.id}">${materia.nombre}</option>
                    </g:each>
                </select>
            </div>
        </div>
        <br/>
        <h1>Horarios disponibles</h1>
        <div class="row">
            <div class="col-md-12">
                <select class="autocompleteSelect2" name="horarios" style="width:100%" multiple="multiple">
                    <g:each in="${horarios.sort()}" var="horario">
                        <option value="${horario.id}" selected>${horario.dia.id}: desde ${horario.rango.inicio} hasta ${horario.rango.fin}</option>
                    </g:each>
                </select>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                <g:submitButton name="create" class="btn btn-primary" value="Recomendar horarios" />
            </div>
        </div>

    </g:form>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".autocompleteSelect2").select2({});
        });

    </script>
</div>
</body>
</html>
