

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'guarani.Usuario'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'guarani.UsuarioRol'
grails.plugin.springsecurity.authority.className = 'guarani.Rol'
grails.plugin.springsecurity.logout.postOnly = false
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',               access: ['permitAll']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/css/**',      access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/**/favicon.ico', access: ['permitAll']],
	[pattern: '/estudiante/create', access: ['permitAll']],
	[pattern: '/estudiante/save', access: ['permitAll']],
	[pattern: '/estudiante/*', access: ['ROLE_ADMIN','ROLE_ESTUDIANTE']],
	[pattern: '/recomendadorMaterias/*', access: ['ROLE_ESTUDIANTE']],
	[pattern: '/organizadorDeHorarios/*', access: ['ROLE_ESTUDIANTE']],
	[pattern: '/simuladorCuatrimestre/*', access: ['ROLE_ESTUDIANTE']]
]
grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/css/**',      filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/**',             filters: 'JOINED_FILTERS']
]

