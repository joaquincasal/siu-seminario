package guarani

class SimuladorCuatrimestreService {

    def estudianteService

    Set<Materia> getMateriasAprobadas(PlanEstudio plan){
        Set<Materia> aprobadas = estudianteService.estudianteLogeado.getMateriasAprobadas(plan)
        return aprobadas
    }

    Set<Materia> getMateriasHabilitadas(PlanEstudio plan, Set<Materia> materiasAprobadas = getMateriasAprobadas(plan)) {
        Set<Materia> habilitadas = estudianteService.estudianteLogeado.getMateriasHabilitadas(plan, materiasAprobadas)
        return habilitadas
    }

    Set<Materia> getMateriasRestantes(PlanEstudio plan, Set<Materia> materiasAprobadas = getMateriasAprobadas(plan)){
        Set<Materia> restantes = estudianteService.estudianteLogeado.getMateriasNoHabilitadas(plan, materiasAprobadas)
        return restantes
    }
}
