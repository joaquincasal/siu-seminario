package guarani

import grails.gorm.transactions.Transactional
import guarani.enums.Dia
import guarani.valueObject.RangoHorario

@Transactional
class OrganizadorDeHorariosService {

    def organizarHorarios(Set<Horario> horariosDisponibles, Set<Materia> materias) {

        OrganizadorDeHorarios organizador = new OrganizadorDeHorarios()
        def catedrasPosibles = this.getCatedrasPosibles(horariosDisponibles,materias)
        def catedras = organizador.organizar(horariosDisponibles,catedrasPosibles)

        return catedras

    }

    def getCatedrasPosibles(Set<Horario> horariosDisponibles, Set<Materia> materias) {
        def catedrasDisponibles = materias.collect { Materia materia -> materia.catedras }

        def catedras = catedrasDisponibles.collect { catedrasPorMateria ->
            catedrasPorMateria.findAll { catedra ->
                catedra.esCursableEnLosHorarios(horariosDisponibles)
            }
        }

        catedras.removeAll { it.size() == 0 }

        return catedras
    }

    Set<Horario> obtenerHorariosSemanales() {

        return Horario.all.findAll {!it.horarioDeCatedra}
        
    }
}
