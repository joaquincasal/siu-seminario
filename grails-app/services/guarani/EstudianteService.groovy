package guarani

import guarani.command.EstudianteCommand

class EstudianteService {

    def springSecurityService

    def crearEstudiante(EstudianteCommand estudianteCommand) {
        Estudiante estudiante = Estudiante.crearEstudiante(estudianteCommand.nombre,
                                                estudianteCommand.apellido,
                                                estudianteCommand.padron,
                                                estudianteCommand.usuario.username,
                                                estudianteCommand.usuario.password)
        return estudiante
    }

    def getMateriasAprobadas(Long id) {

        Estudiante estudiante = Estudiante.get(id)

        return estudiante.materiasAprobadas

    }

    final Estudiante getEstudianteLogeado(){
        return Estudiante.findByUsuario((Usuario)springSecurityService.currentUser)
    }


}
