package guarani

class RecomendadorMateriasService {

    def springSecurityService

    private final Estudiante getEstudianteLogeado(){
        return Estudiante.findByUsuario((Usuario)springSecurityService.currentUser)
    }

    Opinion guardarPuntaje(Long catedraId, Long puntaje, String comentario) {
        Estudiante estudiante = estudianteLogeado
        Catedra catedra = Catedra.get(catedraId)
        return estudiante.opinar(catedra, puntaje, comentario)
    }

    Set<Opinion> obtenerMisOpiniones() {
        Estudiante estudiante = estudianteLogeado
        return estudiante.opiniones
    }

    Set<Interes> obtenerMisIntereses(){
        Estudiante estudiante = estudianteLogeado
        return estudiante.intereses
    }

    def obtenerInteresesNoSeleccionados() {
        Estudiante estudiante = estudianteLogeado
        return Interes.all - estudiante.intereses
    }

    Set<Catedra> getCatedrasParaOpinar() {
        return estudianteLogeado.catedrasCursadasYFinalizadas - estudianteLogeado.opiniones.collect{ it.catedra }
    }

    List<PuntajeCatedra> recomendaciones() {
        Estudiante estudianteBuscador = estudianteLogeado
        Set<Estudiante> estudiantes = Estudiante.findAllByIdNotEqual(estudianteBuscador.id)
        Set<PuntajeEstudiante> puntajesEstudiantes = estudianteBuscador.puntuarEstudiantes(estudiantes)
        Set<Catedra> catedrasDisponibles = estudianteBuscador.materiasRestantes.collect {Materia materia ->
            materia.catedras
        }.flatten()

        Set<PuntajeCatedra> puntajesCatedras = catedrasDisponibles.inject(
                [].toSet()) { Set<PuntajeCatedra> acuPuntajesCatedras, Catedra catedra ->
            acuPuntajesCatedras << this.recomendacion(puntajesEstudiantes, catedra)
        }

        return puntajesCatedras.toList().sort {PuntajeCatedra p1, PuntajeCatedra p2 -> p1.puntaje - p2.puntaje}
    }

    PuntajeCatedra recomendacion(Set<PuntajeEstudiante> puntajesEstudiantes, Catedra catedra){
        /***
         * Vci = sumatoria( Pj * Ocji ) / sumatoria (Pj)
         * donde:
         *      Vci: valor de la catera i para B
         *      Pj: puntaje de referencia del estudiante j para B
         *      Ocji: puntaje de la opinion del estudiante j de la catedra i
         * aclaracion: se toman solo los Pj que hayan cursado en la catedra i en ambas sumatorias
         ***/
        Set<PuntajeEstudiante> puntajesEstudiantesParaCatedra =
                puntajesEstudiantes.findAll{ PuntajeEstudiante puntajeEstudiante ->
                    puntajeEstudiante.estudiante.opinoDeCatedra(catedra)
                }
        Double sumaTodosPuntajes = puntajesEstudiantesParaCatedra.inject(0D) {Double acumulado, puntajeEstudiante ->
            return acumulado + puntajeEstudiante.puntaje
        }
        Double puntajeTotal = puntajesEstudiantesParaCatedra.inject(0D) { Double acu, PuntajeEstudiante puntajeEstudiante ->
            acu += puntajeEstudiante.puntaje *
                    puntajeEstudiante.estudiante.getOpinionParaCatedra(catedra).puntaje
            return acu
        }
        return new PuntajeCatedra(catedra: catedra, puntaje: sumaTodosPuntajes ? puntajeTotal / sumaTodosPuntajes: 0)
    }

    Boolean guardarIntereses(List<Long> interesesId) {
        Estudiante estudiante = estudianteLogeado
        Set<Interes> intereses = Interes.findAllByIdInList(interesesId)
        return estudiante.agregarIntereses(intereses)
    }
}
