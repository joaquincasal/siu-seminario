package guarani

import groovy.transform.Sortable
import guarani.Catedra

/**
 * Created by pablo on 10/12/17.
 */
@Sortable(includes = ['puntaje'])
class PuntajeCatedra {
    Double puntaje
    Catedra catedra
}
