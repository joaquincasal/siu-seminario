package guarani

import org.apache.commons.math3.optim.MaxIter
import org.apache.commons.math3.optim.PointValuePair
import org.apache.commons.math3.optim.linear.*
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType

class OrganizadorDeHorarios {

    int totalDeHoras
    int totalDeCatedras
    int totalDeVariables
    def catedrasDisponibles
    Set<Catedra> listaDeCatedras


    Set<Set<Catedra>> organizar(Set<Horario> horariosDisponibles, def catedrasPosibles) {

        this.inicializar(horariosDisponibles,catedrasPosibles)

        if (this.listaDeCatedras.size() == 0) {
            return []
        }

        Set<LinearObjectiveFunction> funciones = this.generarFuncionesAEvaluar()

        Collection<LinearConstraint> restricciones = new ArrayList<LinearConstraint>()

        restricciones += this.cargarRestriccionesPorCatedras()
        restricciones += this.cargarRestriccionesDeHoras()
        restricciones += this.cargarRestriccionesDeCursada(horariosDisponibles)

        def soluciones = funciones.collect { funcion -> obtenerSolucion(funcion,restricciones) }

        soluciones.removeAll { solucion -> solucion.size() == 0 }

        if(soluciones.size() == 0) {
            return []
        }

        soluciones = soluciones.findAll { ! it.findAll { sol -> sol != 0.0 && sol != 1.0 } }

        Set<Set<Catedra>> catedras = soluciones.collect { solucion ->
            this.obtenerCatedrasSeleccionadas(solucion)
        }

        return catedras

    }

    double[] obtenerSolucion(LinearObjectiveFunction funcion, Collection<LinearConstraint> restricciones) {

        SimplexSolver solucionador = new SimplexSolver()

        try {

            PointValuePair solucion = solucionador.optimize(new MaxIter(100),
                    funcion,
                    new LinearConstraintSet(restricciones),
                    GoalType.MINIMIZE,
                    new NonNegativeConstraint(true))
            solucion.point

        } catch (NoFeasibleSolutionException e) {

            return []

        }

    }

    def inicializar(Set<Horario> horariosDisponibles, def catedrasPosibles) {

        this.catedrasDisponibles = catedrasPosibles
        this.catedrasDisponibles.removeAll { catedras -> catedras.size() == 0 }
        this.listaDeCatedras = catedrasDisponibles.flatten() as Set<Catedra>

        this.totalDeHoras = horariosDisponibles.size()
        this.totalDeCatedras = this.listaDeCatedras.size()
        this.totalDeVariables = this.totalDeHoras + this.totalDeCatedras

    }

    Set<LinearObjectiveFunction> generarFuncionesAEvaluar() {

        //Cargo funciones marcando una catedra con poca prioridad para seleccionarla
        def restriccionDeFunciones = (0..(this.totalDeCatedras-1)).collect { indice ->
            def restriccionesCatedras = this.generarVector(this.totalDeCatedras,0.0)
            restriccionesCatedras  += this.generarVector(this.totalDeHoras,1.0)
            restriccionesCatedras[indice] = 10
            restriccionesCatedras
        }

        Set<LinearObjectiveFunction> funciones = restriccionDeFunciones.collect { funcion ->
            new LinearObjectiveFunction(funcion as double[], 0)
        }

        //Cargo la funcion base
        def valoresAEvaluar = this.generarVector(this.totalDeCatedras,0.0)
        valoresAEvaluar  += this.generarVector(this.totalDeHoras,1.0)

        LinearObjectiveFunction funcionBase = new LinearObjectiveFunction(valoresAEvaluar as double[], 0)

        funciones << funcionBase

        return funciones

    }

    Collection<LinearConstraint> cargarRestriccionesPorCatedras() {

        def restriccionPrevia = []

        def conjuntoDeRestricciones = this.catedrasDisponibles.collect { catedras ->
            def restriccion = restriccionPrevia
            restriccion += this.generarVector(catedras.size(),1.0)
            restriccion +=  this.generarVector((this.totalDeVariables - catedras.size() - restriccionPrevia.size()),0.0)
            restriccionPrevia += this.generarVector(catedras.size(),0.0)

            restriccion
        }

        this.cargarRestriccion(conjuntoDeRestricciones, Relationship.EQ, 1.0)

    }

    Collection<LinearConstraint> cargarRestriccionesDeHoras() {

        def conjuntoDeRestricciones = (this.totalDeCatedras..(this.totalDeVariables-1)).collect { indice ->
            def restriccion = this.generarVector(this.totalDeVariables,0.0)

            restriccion[indice] = 1.0
            restriccion
        }

        this.cargarRestriccion(conjuntoDeRestricciones,Relationship.LEQ, 1.0)

    }

    Collection<LinearConstraint> cargarRestriccionesDeCursada(Set<Horario> horariosDisponibles) {

        def conjuntoDeRestricciones = (0..(this.totalDeHoras-1)).collect { indice ->
            def restriccionDeHorarios = this.generarVector(this.totalDeHoras,0.0)
            restriccionDeHorarios[indice] = -1.0

            def restriccion = this.listaDeCatedras.collect { catedra ->
                int seCursa = catedra.seCursaEnElHorario(horariosDisponibles[indice]) ? 1.0 : 0.0
                seCursa
            }
            restriccion += restriccionDeHorarios
            restriccion

        }

        this.cargarRestriccion(conjuntoDeRestricciones,Relationship.EQ, 0.0)
    }

    Set<Catedra> obtenerCatedrasSeleccionadas(double[] solucion) {

        def catedras = (0..(this.totalDeCatedras-1)).inject([]) { resultado, indice ->
            if (solucion[indice]){
                resultado << this.listaDeCatedras[indice]
            }
            resultado
        }

        return catedras as Set<Catedra>

    }

    Collection<LinearConstraint> cargarRestriccion(def conjuntoDeRestricciones, Relationship relacion, double valor) {

        Collection<LinearConstraint> restricciones = new ArrayList<LinearConstraint>()

        conjuntoDeRestricciones.inject(restricciones) { total, restriccion ->
            restricciones.add(new LinearConstraint(restriccion as double[], relacion, valor))
        }

        return restricciones

    }

    def generarVector(def fin, double valor) {
        return (1..fin).collect {valor}
    }
}
