package guarani.valueObject

import grails.validation.Validateable
import groovy.transform.Sortable

/**
 * Created by pablo on 20/11/17.
 */
@Sortable(includes = ['inicio', 'fin'])
class RangoHorario implements Validateable{

    Long inicio
    Long fin

    static constraints = {
        inicio nullable: false, min:7L, Max:23L
        fin nullable: false, min:7L, Max:23L, validator: { fin, rango ->
            return  fin > rango.inicio
        }
    }

    RangoHorario(){}

    RangoHorario(Long inicio, Long fin){
        if(fin <= inicio)
            throw Exception() //TODO: mejorar esto :'(
        this.inicio = inicio
        this.fin = fin
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        RangoHorario that = (RangoHorario) o

        if (fin != that.fin) return false
        if (inicio != that.inicio) return false

        return true
    }

    int hashCode() {
        int result
        result = inicio.hashCode()
        result = 31 * result + fin.hashCode()
        return result
    }

    boolean pertenece(RangoHorario rangoHorario) {
        return this.inicio <= rangoHorario.inicio && this.fin >= rangoHorario.fin
    }

    int cantidadDeHoras() {
        return this.fin - this.inicio
    }

}
