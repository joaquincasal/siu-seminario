package guarani.enums

/**
 * Created by pablo on 20/11/17.
 */
enum Dia {
    Lunes("Lunes"),
    Martes("Martes"),
    Miercoles("Miercoles"),
    Jueves("Jueves"),
    Viernes("Viernes"),
    Sabado("Sabado")

    String id
    Dia(id){
        this.id = id
    }
}